import { test, expect } from '@playwright/test';
import LoginPage from './pageObjects/LoginPage';


test.beforeEach(async ({ page }) => {
    const loginPage = new LoginPage(page);
    await loginPage.login('standard_user', 'secret_sauce');
});

test('Login and verify Elements are visible', async ({ page }) => {
    await expect(page.locator('[data-test="product-sort-container"]')).toBeVisible();
    await expect(page.locator('[data-test="product-sort-container"]')).toContainText('Name (A to Z)');
    await expect(page.locator('[data-test="item-4-title-link"] [data-test="inventory-item-name"]')).toContainText('Sauce Labs Backpack');
    await expect(page.locator('[data-test="add-to-cart-sauce-labs-backpack"]')).toBeVisible();
});

test('Verify products are sorted A-Z', async ({ page }) => {
    await expect(page.locator('[data-test="item-4-title-link"] [data-test="inventory-item-name"]')).toContainText('Sauce Labs Backpack');
    await expect(page.locator('[data-test="item-0-title-link"] [data-test="inventory-item-name"]')).toContainText('Sauce Labs Bike Light');
    await expect(page.locator('[data-test="item-1-title-link"] [data-test="inventory-item-name"]')).toContainText('Sauce Labs Bolt T-Shirt');
});

test('Change sorting to Z-A then verify', async ({ page }) => {
    await page.click('[data-test="product-sort-container"]');
    await page.selectOption('[data-test="product-sort-container"]', 'za');
    await expect(page.locator('[data-test="product-sort-container"]')).toContainText('(Z to A)');
    await expect(page.locator('[data-test="item-3-title-link"] [data-test="inventory-item-name"]')).toContainText('Test.allTheThings() T-Shirt (Red)');
    await expect(page.locator('[data-test="item-2-title-link"] [data-test="inventory-item-name"]')).toContainText('Sauce Labs Onesie');
    await expect(page.locator('[data-test="item-5-title-link"] [data-test="inventory-item-name"]')).toContainText('Sauce Labs Fleece Jacket');
});

test('Verify items are then sorted Z-A', async ({ page }) => {
    await page.click('[data-test="product-sort-container"]');
    await page.selectOption('[data-test="product-sort-container"]', 'za');
    await expect(page.locator('[data-test="product-sort-container"]')).toContainText('(Z to A)');
    await expect(page.locator('[data-test="item-3-title-link"] [data-test="inventory-item-name"]')).toContainText('Test.allTheThings() T-Shirt (Red)');
    await expect(page.locator('[data-test="item-2-title-link"] [data-test="inventory-item-name"]')).toContainText('Sauce Labs Onesie');
    await expect(page.locator('[data-test="item-5-title-link"] [data-test="inventory-item-name"]')).toContainText('Sauce Labs Fleece Jacket');
});
