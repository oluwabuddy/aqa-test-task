import { test, expect } from '@playwright/test';
const API_URL = 'https://jsonplaceholder.typicode.com/posts';


// creating a new post
const newPostPayload = {
  userId: 11,
  id: 2,
  title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
  body: 'quia et recusandae consequuntur expedita et molestiae ut ut quas erum est autem sunt rem eveniet architecto',
};
// updating a post
const updatedPostPayload = {
  userId: 11,
  id: 2,
  title: 'ea molestias quasi exercitationem repellat qui ipsa sit aut',
  body: 'et iusto sed quo iurluptatem occaecati omnis eligendi aut aoluptatem doloribus vel accusantium quis parilestiae porro eius odio et labore et velit',
};



//  create a post
async function createPost(request, payload) {
  const response = await request.post(`${API_URL}`, {
    data: payload,
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
  });
  await expect(response.ok()).toBeTruthy();
  await expect(response.status()).toBe(201);
  const createdPost = await response.json();
  console.log('Created post:', createdPost);
  return createdPost.id;
}

// function to retrieve a post by ID
async function getPostById(request, postId) {
  const response = await request.get(`${API_URL}/${postId}`);
  await expect(response.status()).toBe(404); // Check status code only
  const retrievedPost = await response.json();
  console.log('Retrieved post:', retrievedPost);
}

// create a new post
test('Create a new post', async ({ request }) => {
  const postId = await createPost(request, newPostPayload);
  console.log('Created post ID:', postId);
});

//  update the created post
test('Update the created post', async ({ request }) => {
  const postId = await createPost(request, newPostPayload);
  await request.patch(`${API_URL}/${postId}`, {
    data: updatedPostPayload,
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
  });
  console.log('Updated post:', updatedPostPayload);
});

// delete the created post
test('Delete the created post', async ({ request }) => {
  const postId = await createPost(request, newPostPayload);
  await request.delete(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
  });
  console.log('Post deleted successfully.');

  // Attempt to get the deleted post (expecting a 404 status)
  await getPostById(request, postId);
});

// Test to check the number of posts for integrity

test('Check the number of posts to ensure integrity', async ({ request }) => {
  const response = await request.get(`${API_URL}`);
  await expect(response.ok()).toBeTruthy();
  await expect(response.status()).toBe(200);
  const currentPosts = await response.json();
  const currentTotal = currentPosts.length;
  console.log('Current total posts:', currentTotal);
});

