import { Page } from 'playwright';

class loginPage {
 private page: Page;

 constructor(page: Page) {
    this.page = page;
 }

 async login(username: string, password: string): Promise<void> {
    await this.page.goto('https://www.saucedemo.com');
    await this.page.fill('[data-test="username"]', username);
    await this.page.fill('[data-test="password"]', password);
    await this.page.click('[data-test="login-button"]');
 }
}

export default loginPage;
