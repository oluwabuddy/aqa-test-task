# AQA Test task

This repository hosts automated tests for web applications. It utilizes Playwright with TypeScript for UI and API automation, and Mochawesome for generating test reports. The tests cover a wide range of functionalities, including login, product sorting, item visibility, and API CRUD operations.

## Getting started


### Prerequisites

Ensure you have Node.js installed on your system.

### Installation

1. Clone the repository to your local machine:
git clone https://gitlab.com/oluwabuddy/aqa-test-task.git

2. Navigate to the project directory:

3. Install the project dependencies:
npm install

## Running the Tests

To run the tests, execute the following command in your terminal:
npx playwright test

This command will run the tests using Playwright and generate a test report.

## Test Structure

The tests are organized into two main categories: UI tests and API tests, and the Page Object Model is used to make the code scalable.

### UI Tests

The UI tests verify the functionality and appearance of various elements on the application's pages, including login functionality, product sorting, and item visibility. These tests use the Page Object Model (POM) pattern to encapsulate page-specific logic and make the tests more maintainable and readable.


### UI Test Cases
1. **Login and Verify Items Sorting:**
   - **Description:** Verify that user can log in to the site and items are sorted by Name (A -> Z).
   - **Steps:**
     1. Go to [Sauce Demo](https://www.saucedemo.com/).
     2. Log in to the site.
     3. Verify that the items are sorted by Name (A -> Z).
   - **Expected Result:** User should be able to log in successfully, and items should be sorted alphabetically from A to Z.

2. **Change Sorting to Name (Z -> A) and Verify:**
   - **Description:** Verify that user can change the sorting to Name (Z -> A) and items are sorted correctly.
   - **Steps:**
     1. Change the sorting to Name (Z -> A).
     2. Verify that the items are sorted correctly.
   - **Expected Result:** Sorting should be changed successfully, and items should be sorted alphabetically from Z to A.


### UI Tests:

To run the UI tests, follow these steps:

- **1. Clone the repository to your local machine.

- **2. Navigate to the `ui-tests` directory.

- **3. Install the project dependencies.

4. Run the tests.
- **Login and verify Elements are visible:** Verifies that after logging in, certain elements are visible on the page and contain the expected text.
- **Verify products are sorted A-Z:** Checks that the products are initially sorted in ascending order by name.
- **Change sorting to Z-A then verify:** Tests the functionality to change the product sorting order to descending (Z-A) and verifies the order.
- **Verify items are then sorted Z-A:** Similar to the previous test, but explicitly tests the Z-A sorting functionality.

### API Tests

The API tests focus on verifying the CRUD operations on posts. These tests use Playwright's request fixture to make HTTP requests to the provided API endpoints and validate the responses.

### API Tests:

- **Create a new post:** Tests the creation of a new post and verifies the response.
- **Update the created post:** Tests the updating of a post and verifies the response.
- **Delete the created post:** Tests the deletion of a post and verifies the response.
- **Check the number of posts to ensure integrity:** Verifies the total number of posts to ensure data integrity.


